[![black](https://img.shields.io/badge/code%20style-black-black)](https://pypi.org/project/black/)

- [Intro](#intro)
- [Installation](#installation)
- [Configuration](#configuration)
- [REDCap configuration](#redcap-configuration)
  - [Source project](#source-project)
  - [Target project](#target-project)
  - [Logging project](#logging-project)
- [Execution](#execution)
  - [Enviroment configuration](#enviroment-configuration)
- [Error reporting](#error-reporting)
- [Limitations](#limitations)
- [Citing](#citing)

## Intro

`NU-sync` is a module that will copy data from a source REDCap project into a target project.  The results of this copy will be logged in a third project, which can be used to sent email or SMS notification of the results.  It is called by a cron job or scheduled task.

Source code belongs to Northwestern and MSU Biomedical Research Informatics Core.  Either can extend it as needed.

## Installation

The module requires a version of python with a current SSL library and a version of pip that can handle a `pyproject.toml` file.  This was developed on python 3.10 and pip 21.2.

Python allows for virtual environments on a single machine.  An environment called `NU` can be created and then activated with a current version of python using

```sh
$ python3.10 -m venv ~/NU
$ source ~/NU/bin/activate
```

The module can then be installed when in the directory containing the code using.

```sh
(NU)$ python3 -m pip install .
```


## Configuration

This module is configured using the `config.ini` file.  It allows for multiple copies to be performed if there are many projects that need this functionality. Each copy will have its own section of the configuration file. These configurations can reference the same projects multiple times, so you can have the following copies set up. They need to be included in the preferred order in the configuration.

* **A&rarr;B** and **A&rarr;C** 
* **A&rarr;B** and **B&rarr;C** 

The following is an example configuration for a single copy task named **Production**.

```ini
[Production]
source_url:https://redcap.bric.msu.edu/api/
target_url:https://redcap.bric.msu.edu/api/
source_token_key:SOURCE
target_token_key:TARGET
error_token_key:LOG_TOKEN
copy_on_true_field:copy
send_all:0
date_sent_field:date_sent
log_field:copy_log
target_event_map:[{
    "0":"enrollment_arm_1",
    "1":"enrollment_arm_2"
    }]
event_field:randomization    
copy_fields:[
    "sex",
    "t"
    ]
copy_forms:[
    "interview_summary"
    ]
```

**source_url** *(required)* &mdash; API URL of the source project.  Also used as the URL for the logging project since the source project drives the process. 

**target_url** *(required)* &mdash; API URL of the target project.  This can be different from the source_url.

**source_token_key, target_token_key, error_token_key** *(required)* &mdash; names of the environment variables that contain the API tokens.  Details on setting environment variables is included in the [Execution](#execution) section 

**copy_on_true_field** *(required)* &mdash; Name of the field that must be true for a copy of data to happen for a record.  This is typically going to be a calculated field so the complex logic of determining when a record is ready to be copied is all tracked in the metadata.

**send_all** &mdash; Boolean indicating if all the records should be sent on each occurrence  of the copy process.  This should be 1 if modifications to the source project are updating the target project.  For example, if an update to the email for a record happens in the source project, this value should be true to make sure the target is updated.  If the copy only happens one time, the value should be 0.

**date_sent_field** *(required if send_all is true)* &mdash; Name of the field that will hold the date of when the record was sent to the target project.  This is used to ensure the data are only sent one time per record.  If data are only sent one time, but a resend is desired, this field can be sent to empty again.

**event_field** *(required)* &mdash; Name of the field that determines the arm of the record when it is copied into the target project.  The field should be numeric and works in conjunction with the **target_event_map**.  This allows for setting the arm in the target project based on a randomization field.

**target_event_map** *(required)* &mdash; A mapping from the values of the **event_field** to the unique event names in the target project.  This allows the module to set the appropriate arm for the record.

**copy_field** *(one or more)* &mdash; List of field names that will be copied to the target project.  The field names need to match exactly between the source and target projects.

**copy_form** *(zero or more)* &mdash; List of form names that will be copied in their entirety to the target project.  The field names on the source form need to match exactly to the target project, although the forms do not need to be named the same or divided in the same way.

## REDCap configuration

There are a few required configuartions that need to be made on the REDCap side of the installation

### Source project
* Import/Export API configured
* Definition of fields specified in  *copy_on_true_field*, *date_sent_field*, and *event_field*

### Target project
* Import/Export API configured
* Definition of all fields included in *copy_fields* and all fields on *copy_forms*

### Logging project 
* Import only API configured
* Creation of logging project using with the provided *logging_project.cdisc* file in the repository.  

## Execution

The module is intended to be run as a cron job that runs on a schedule that works for the study.  There is an entry in the logging project for each execution, so it should not be execute more than once an hour.   Execution more often may have a negative impact on performance.  

### Enviroment configuration
The module uses environmental variables that need to be set for the user that is executing the cron job. In Linux this is done with the following in the users `.bashrc`.

```sh
export LOG_TOKEN=4F9ADD...
export SOURCE=AA8A00...
export TARGET=28364A...
```  

In Windows you can access the Environment Variables dialog, or use the *setx* command

```sh
setx LOG_TOKEN 4F9ADD...
setx SOURCE AA8A00...
setx TARGET 28364A...
```  

An example script is included in *runSync.sh*.  This runs on a Mac environment and ensures the environment variables are set, moves to the correct directory, and then executes the module.

## Error reporting

All results are logged to the logging project.  This can be used to add notifications of specific events.  These events can be detected with the following logic.

**Failure** &mdash; [type] = 1 

**Copy including one or more records** &mdash; !starts_with( [message], "0 records updated:")

## Limitations 
 
- The module is not built to handle a single record at a time, so it can not currently be run on an individual basis.
- The code is intended to be called from a cron job.  It can not be called in real time as records are updated in REDCap.  There is no web service involved in this so it cannot be called by REDCap through a trigger.  This was done because NU did not want to build in the level of complexity that is involved in creating a web service.
- The source project is expected to only have a single event.  Multiple arms or events may lead to unexpected behavior.
- Time of execution is not configurable in the module.  All copy configurations would execute at the same time.  This may not work well for multiple different project that have different needs.


## Citing

This module uses PyCap.  If you use PyCap in your research, please consider citing the software:

>    Burns, S. S., Browne, A., Davis, G. N., Rimrodt, S. L., & Cutting, L. E. PyCap (Version 1.0) [Computer Software].
>    Nashville, TN: Vanderbilt University and Philadelphia, PA: Childrens Hospital of Philadelphia.
>    Available from https://github.com/redcap-tools/PyCap. doi:10.5281/zenodo.9917