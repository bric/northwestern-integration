import os
import json
import configparser

# import projectfunctions as pf
import record_mover as rm


config = configparser.ConfigParser()
config.read("config.ini")
for project in config.sections():
    mover = rm.RecordMover(config[project], project)
    records = mover.get_valid_records()
    print(project)
    print(records)

    records = mover.set_target_event(records)
    mover.write_update(records)
