from redcap.methods.records import Records
from redcap import Project
from requests import RequestException
import error_logger as err
import json
import os
import datetime
from pprint import pprint


class RecordMover:
    def __init__(self, config, section) -> None:
        self.source_url = config["source_url"]

        self.source_token = os.getenv(config["source_token_key"])
        self.target_url = config["target_url"]
        self.target_token = os.getenv(config["target_token_key"])
        self.copy_on_true_field = config["copy_on_true_field"]
        self.copy_fields = json.loads(config.get("copy_fields", "{}"))
        self.copy_forms = json.loads(config.get("copy_forms", "{}"))
        self.send_all = bool(int(config.get("send_all", "0")))
        self.date_sent_field = config.get("date_sent_field", "")
        self.event_field = config.get("event_field", None)
        self.target_event_map = json.loads(config.get("target_event_map", "{}"))[0]

        self.err = err.ErrorLogger(
            config["source_url"], config.get("error_token_key", ""), section
        )

        if self.source_token is None:
            self.err.die("Source token not specified")
        if self.target_token is None:
            self.err.die("Target token not specified")
        if self.source_url is None:
            self.err.die("Source url not specified")
        if self.target_url is None:
            self.err.die("Target url not specified")

        p_tar = Project(self.target_url, self.target_token)
        events = p_tar.export_instrument_event_mappings()
        self.target_event_name = events[0]["unique_event_name"]
        self.target_id_field = p_tar.def_field

        p_src = Project(self.source_url, self.source_token)
        self.source_id_field = p_src.def_field

        self.copy_fields.append(self.source_id_field)

    def get_valid_records(self):
        filter = "[" + self.copy_on_true_field + "]=1"
        if self.send_all is False:
            if self.date_sent_field == "":
                self.err.die(
                    "No date sent field specified, but is required if not sending all records."
                )
            filter = filter + " and [" + self.date_sent_field + '] = ""'
        records = Records(self.source_url, self.source_token)

        try:
            data = records.export_records(
                filter_logic=filter,
                fields=self.copy_fields + [self.event_field],
                forms=self.copy_forms,
            )
        except RequestException as e:
            self.err.die("Failed to query records\n" + str(e))
        return data

    def get_event_name(self, record):
        if len(self.target_event_map) == 0:
            return self.target_event_name
        else:
            return self.target_event_map[record[self.event_field]]

    def remove_field(self, data, field_to_drop):
        result = []
        for record in data:
            new_record = {}
            for field in record:
                if field != field_to_drop:
                    new_record[field] = record[field]
            result.append(new_record)
        return result

    def set_target_event(self, data):
        for record in data:
            record["redcap_event_name"] = self.get_event_name(record)
        return data

    def add_field_with_value(self, data, field, value):
        for record in data:
            record[field] = value
        return data

    def rename_field(self, data, source_field, target_field):
        if source_field == target_field:
            return data
        for record in data:
            record[target_field] = record[source_field]
            del record[source_field]
        return data

    def write_update(self, data):
        target_data = self.remove_field(data, self.event_field)
        target_data = self.rename_field(
            target_data, self.source_id_field, self.target_id_field
        )

        records = Records(self.target_url, self.target_token)
        try:
            res = records.import_records(target_data)
            self.err.log(
                str(res["count"])
                + " records updated:\n\n"
                + "\n".join([r[self.target_id_field] for r in target_data])
            )
        except RequestException as e:
            print(target_data)
            self.err.die("Failed to write records\n" + str(e))

        updates = [
            {i[0]: i[1]}
            for d in data
            for i in d.items()
            if i[0] == self.source_id_field
        ]

        if not self.send_all:
            updates = self.add_field_with_value(
                updates,
                self.date_sent_field,
                datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            )
            update_src = Records(self.source_url, self.source_token)
            try:
                res = update_src.import_records(updates)
            except RequestException as e:
                self.err.die(
                    "Failed to update source project with sent date\n" + str(e)
                )
