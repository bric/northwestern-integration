from redcap.methods.records import Records
import json
import os
import datetime


class ErrorLogger:
    def __init__(self, url, token_key, section):
        self.token = os.getenv(token_key)
        self.enabled = False
        if self.token is None:
            print("No logging enabled for section " + section)
        else:
            self.enabled = True
            self.records = Records(url, self.token)

    def die(self, message):
        self.log(message, 1)
        raise Exception(message)

    def log(self, message, type=0):
        if self.enabled:
            record = {}
            record["record_id"] = self.records.generate_next_record_name()
            record["ts"] = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
            record["type"] = type
            record["message"] = message
            self.records.import_records([record])
